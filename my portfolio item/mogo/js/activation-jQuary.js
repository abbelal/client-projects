/* scrollUp Minimum setup */
$(function () {
  $.scrollUp({
	scrollName: 'scrollUp', // Element ID
	topDistance: '500', // Distance from top before showing element (px)
	topSpeed: 700, // Speed back to top (ms)
	animation: 'slide', // Fade, slide, none
	animationInSpeed: 500, // Animation in speed (ms)
	animationOutSpeed: 500, // Animation out speed (ms)
	scrollText: 'Scroll to top', // Text for element
	activeOverlay: true, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});


/* jQuery counterUp plugin */
jQuery(document).ready(function( $ ) {
	$('.counter').counterUp({
	//delay: 10, // the delay time in ms
	time: 800 // the speed time in ms
	});

});


/* accordion js */
(function($) {
	var allPanels = $('.accordion > dd').hide();
	$('.accordion > dd:first-of-type').show();
	$('.accordion > dt:first-of-type').addClass('accordion-active');
	jQuery('.accordion > dt').on('click', function() {
		$this = $(this);
		$target = $this.next(); 
		if(!$this.hasClass('accordion-active')){
			$this.parent().children('dd').slideUp();

			jQuery('.accordion > dt').removeClass('accordion-active');
			$this.addClass('accordion-active');
			$target.addClass('active').slideDown();

		}    

		return false;
	});

})(jQuery);


/* masonry activation for porfolio */
$( window ).load( function()
{
    $( '#portfolio-wrapper' ).masonry({ 
	itemSelector: '.single-porfolio' 
	});
	
	
});


/* for wow js activation */
new WOW().init();



